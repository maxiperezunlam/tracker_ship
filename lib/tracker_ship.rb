# frozen_string_literal: true

require_relative "tracker_ship/version"

# Incluimos todos los tracker_state_mappers y tracker_pollers
Dir["./tracker_ship/tracker_*/*.rb"].each do |dep|
	require_relative dep
end

Dir["./tracker_ship/configs/*.rb"].each do |dep|
	require_relative dep
end


module TrackerShip
	#Empty module
end

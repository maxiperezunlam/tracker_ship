module TrackerShip
	module Status
		class Success 
			attr_reader :tracking_number
			attr_reader :delivery_status
			
			def initialize(tracking_number, delivery_status)
				@tracking_number = tracking_number
				@delivery_status = delivery_status
			end
		end
	end
end

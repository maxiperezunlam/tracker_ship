module TrackerShip
	module Status
		class Error < StandardError
			attr_reader :tracking_number	
			
			def initialize(tracking_number, message)
				super(message)
				@tracking_number = tracking_number
			end
		end
	end
end

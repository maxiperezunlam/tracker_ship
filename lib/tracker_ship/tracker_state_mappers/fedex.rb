# frozen_string_literal: true

require_relative './base'

module TrackerShip
	module TrackerStateMapper
		class Fedex < TrackerShip::TrackerStateMapper::Base
			# @return [TrackerShip::TrackingState] The tracking status for the specified package tracking number
			def states
				created_status = 'Shipment information sent to fedex' 			
				at_pickup = 'At Pickup' 
				picked_up = 'Picked Up' 
				arrived_at_fedex = 'Arrived at Fedex location' 
				at_fedex_dest = 'At Fedex destination facility' 
				at_dest_sort = 'At destination sort facility' 
				departed = 'Departed Fedex location' 
				on_vehicle = 'On Fedex vehicle for delivery' 
				international_release = 'International shipment release' 
				ready_for_pickup = 'Ready for pickup' 
				delivered = "Delivered" 
				delivery_exception = 'Delivery Exception' 
				shipment_exception = 'Shipment Exception'
				clearance = 'Clearance Delay' 
				cancelled = 'Shipment cancelled by sender' 

				@states ||= {
					created_status => TrackerShip::TrackingState::Created.new(created_status),
					at_pickup => TrackerShip::TrackingState::OnTransit.new(at_pickup),
					picked_up => TrackerShip::TrackingState::OnTransit.new(picked_up),	
					arrived_at_fedex => TrackerShip::TrackingState::OnTransit.new(arrived_at_fedex),	
					at_fedex_dest => TrackerShip::TrackingState::OnTransit.new(at_fedex_dest),
					at_dest_sort => TrackerShip::TrackingState::OnTransit.new(at_dest_sort),
					departed => TrackerShip::TrackingState::OnTransit.new(departed),
					on_vehicle => TrackerShip::TrackingState::OnTransit.new(on_vehicle),
					international_release => TrackerShip::TrackingState::OnTransit.new(international_release), 
					ready_for_pickup => TrackerShip::TrackingState::OnTransit.new(ready_for_pickup),
					delivered => TrackerShip::TrackingState::Delivered.new(delivered),	
					delivery_exception => TrackerShip::TrackingState::Exception.new(delivery_exception),
					shipment_exception => TrackerShip::TrackingState::Exception.new(shipment_exception),
					clearance => TrackerShip::TrackingState::Exception.new(clearance),
					cancelled => TrackerShip::TrackingState::Exception.new(cancelled)
				}
			end
		end	
	end
end

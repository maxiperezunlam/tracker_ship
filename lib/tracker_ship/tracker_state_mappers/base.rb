# frozen_string_literal: true

require 'dry-monads'
require_relative '../tracking_states/all'

module TrackerShip
	module TrackerStateMapper
		class Base
			include Dry::Monads[:result]

			def map(current_state)
				state = states.find do |key,value|
					current_state.strip.match(%r{\A#{key}\z}i)
				end

				if state.nil?
					Failure(TrackerShip::TrackingState::MappingError)
				else
					current_state = state.last
					
					current_state.exception? ? Failure(current_state) : Success(current_state)
				end
			end

			protected
			
			def states
				raise NotImplementedError, "#{self.class} must implement the method '#{__method__}'"
			end	
		end
	end
end

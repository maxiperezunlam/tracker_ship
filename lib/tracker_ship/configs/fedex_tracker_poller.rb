require_relative './base'

module TrackerShip
	module Config
		class FedexTrackerPoller < Base

			# Para la implementacion de ejemplo simplemente usamos un objeto que hereda desde Config::Base.
			# Esta a su vez hereda de la implementacion default de Hash. Indicamos los parametros necesarios
			# como valores por defecto. No obstante en caso de requerir la carga externa de los datos
			# en la clase base pueden implementarse metodos de clase que carguen los datos desde diferentes formatos de archivo
			# o bien utilizar y configurar AnywayConfig por ejemplo para que se encargue por nosotros del trabajo. 
			# Otra alternativa es utilizar dotenv y cargar todos los valores desde un archivo .env con variables de entorno.
			def initialize(key: "VZ0tu2xxC4LKxZY6", 
				    password: "AKOh8wjdYsJtNI6CFKaxPFLka", 
				    account_number: 802388543, 
				    meter: 100495015,
				    mode: "test")
				self[:key] = key
				self[:password] = password
				self[:account_number] = account_number
				self[:meter] = meter
				self[:mode] = mode
			end
		end
	end
end

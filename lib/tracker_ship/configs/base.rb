# frozen_string_literal: true

# For more information read the next article:
#	* https://evilmartians.com/chronicles/anyway-config-keep-your-ruby-configuration-sane
module TrackerShip
	module Config
		class Base < Hash 
			class << self
				def instance
					@instance ||= new
				end

				private
				
				def respond_to_missing?(name, include_private=false)
					instance.respond_to?(name, include_private)
				end
				
				def method_missing(method, *args, &block)
					instance.send(method, *args, &block)
				end	
			end
		end
	end
end

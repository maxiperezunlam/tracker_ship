# frozen_string_literal: true

module TrackerShip
	module TrackingState; end
end

require_relative './base.rb'
require_relative './created.rb'
require_relative './delivered.rb'
require_relative './on_transit.rb'
require_relative './exception.rb'
require_relative './mapping_error.rb'

require_relative './base'

module TrackerShip
	module TrackingState
		class MappingError < Base
			def mapping_error?
				true
			end
		end
	end
end

require_relative './base'

module TrackerShip
	module TrackingState
		class Exception < TrackerShip::TrackingState::Base
			def exception?
				true
			end
		end
	end
end

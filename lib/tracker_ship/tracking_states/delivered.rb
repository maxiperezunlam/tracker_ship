require_relative './base'

module TrackerShip
	module TrackingState
		class Delivered < TrackerShip::TrackingState::Base
			def delivered?
				true
			end
		end
	end
end

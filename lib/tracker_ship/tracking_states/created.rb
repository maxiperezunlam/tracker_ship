require_relative './base'

module TrackerShip
	module TrackingState
		class Created < TrackerShip::TrackingState::Base
			def created?
				true
			end
		end
	end
end

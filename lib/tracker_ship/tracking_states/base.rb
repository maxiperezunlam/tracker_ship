module TrackerShip
	module TrackingState
		class Base
			attr_reader :description

			def initialize(description)
				@description = description
			end

			def exception?
				false
			end

			def delivered?
				false
			end
	
			def on_transit?
				false
			end
		
			def created?
				false
			end
			
			def error?
				false
			end
		end
	end
end

require_relative './base'

module TrackerShip
	module TrackingState
		class OnTransit < TrackerShip::TrackingState::Base
			def on_transit?
				true
			end
		end
	end
end

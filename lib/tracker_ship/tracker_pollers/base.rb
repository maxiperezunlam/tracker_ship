require 'em-synchrony'
require "em-synchrony/fiber_iterator"
require 'dry-monads'
require_relative './errors.rb'
require_relative '../statuses/success.rb'
require_relative '../statuses/error.rb'

module TrackerShip
	module TrackerPoller
		# Template class para todos los TrackerPoller. Esta implementa una interfaz comun donde sobrescribiendo los 
		# siguientes metodos, deberia de poder implementar un TrackerPoller concreto:
		#	* api_tracking_object [ConcreteShippingApiProvider] factory method para construir la clase concreta 
		#							    que wrapea la conexion a la api externa
		#	* api_tracking_method_name [String] cadena que indica el nombre del metodo de la clase indicada por 
		#					    api_tracking_object para consultar el estado de un envio.
		#	* api_tracking_method_params [Hash] hash que indica cada uno de los parametros necesarios a ser indicados 
		#					    en la llamada al metodo indicado por api_tracking_method_name.
		#       * api_extract_delivery_status [String] metodo para extrar la informacion del estado de envio a partir 
		#					       del resultado obtenido al llamar la api del proveedor
		#	* delivery_status_mapper [TrackerStateMapper] factory method para obtener el TrackerStateMapper
		#						      asociado al proveedor de logistica
		# Con la gema se adjunta la implementacion para Fedex, no obstante se es libre de extender desde esta clase
		# e implementar la logica necesaria para otro proveedor de logistica.
		# Ademas se incluye como facilidades de la clase:
		#	* Implementacion de retries para manejar transient faults.
		#	* Manejo de errores utilizando la monada Result
		class Base
			include Dry::Monads[:result]
			
			DEFAULT_CONCURRENCY = 4
			MAX_RETRIES_PER_REQUEST = 3
			MAX_BACKOFF_TIME_IN_SECONDS = 32

			attr_reader :configuration_service, :tracking_number_fetcher, :concurrency

			def initialize(configuration_service,
				       tracking_number_fetcher,	
				       concurrency = DEFAULT_CONCURRENCY,
				       logger = nil)
				@tracking_number_fetcher = tracking_number_fetcher
				@concurrency = concurrency
				@configuration_service = configuration_service
				@logger = logger || Logger.new(STDOUT)
			end

			def call
				api = api_tracking_object
				delivery_results = []
				requests = build_requests_info

			        fiber_concurrency = [@concurrency, requests.size].min
	
				EM.synchrony do
					EM::Synchrony::FiberIterator.new(requests, fiber_concurrency).each do |request|
						# Por cada request reintentamos una cantidad preestablecida de veces
						# por si existen transient faults de conexion con la api de los proveedores
						tracking_number = request[:tracking_number]
						tracking_method = request[:method]
						@logger.debug("tracking_number: #{tracking_number}")

						delivery_results << try_get_api_result_for(tracking_number) do
							_result = api.__send__(tracking_method, 
								       	       api_tracking_method_params(tracking_number))
							Success([tracking_number, api_extract_delivery_status(_result)])
						end
					end
					
					EM.stop
				end		
				
				homogenize_statuses(delivery_results)
			end

			# Implementacion por defecto para intentar obtener el resultado de la api
			# Notese que la implementacion siempre debe de hacer uso de yield para garantizar un
			# manejo homogeneo de comunicacion contra una api concreta. El sentido de este metodo
			# es modificar  en caso de necesitarlo la implementacion de reintentos y/o el mapeo
			# de los diferentes errores que se pueden obtener al realizar un request de la API. 
			# Por ejemplo mapear excepciones externar a tipos de errores concretos de la capa de negocio.
			def try_get_api_result_for(tracking_number)
				retries = 0
			
				begin	
					return yield
				rescue NotImplementedError => e
					raise e
				rescue StandardError => e
					retries = [retries + 1, MAX_RETRIES_PER_REQUEST].min	
					if retries >= MAX_RETRIES_PER_REQUEST
						return Failure([tracking_number, e])
					else
						EM::Synchrony.sleep(exponential_backoff(retries))
						retry
					end
				end
			end
	
			# Obtiene un valor para el exponential backoff. Esta es la implementacion default pero puede sobreescribirse 
			# el metodo en caso de necesitar una implementacion diferente.
			def exponential_backoff(n)
				n = [n, 0].max #De esta manera garantizamos que nunca el minimo pueda ser inferior a 1 segundo.
				random_number_of_seconds = [*0..32].sample
				[(2**n) + random_number_of_seconds, MAX_BACKOFF_TIME_IN_SECONDS].min
			end

			protected 
			
			# A partir de la informacion retornada por el tracking_number_fetcher inyectado
			# construye un hash de hashes que contiene la informacion necesaria para ejecutar 
			# cada uno de los requests en cada una de las fibras.
			def build_requests_info
				requests = {}	
				@tracking_number_fetcher.call.map do |tracking_number|
					requests["req-#{tracking_number}"] = {
						method: api_tracking_method_name,
						tracking_number: tracking_number
					}
				end
			end

			def homogenize_statuses(delivery_results)
				mapper = delivery_status_mapper 
				delivery_results.map do |delivery_result|
					case delivery_result
					in Dry::Monads::Result::Success
						begin
						  delivery_result.bind do |status|
						  	TrackerShip::Status::Success.new(status.first, mapper.map(status.last))
						  end
						rescue StandardError => e
						  delivery_result.bind do |status|
							  TrackerShip::Status::Error.new(status.first, e.message)
						  end
						end
					in Dry::Monads::Result::Failure
						delivery_result.bind do |error|
							TrackerShip::Status::Error.new(error.first, error.last)
						end
					end
				end
			end

			# Retorna un symbol que identifica el nombre dentro del objecto api que deberá de ser invocado
			# para obtener la informaciòn de seguimiento del paquete.
			def api_tracking_method_name
				raise NotImplementedError, "#{self.class} must implement the method '#{__method__}'"
			end

			# Retorna el estado en el formato esperado por el mapper. Ya que cada api puede tener una logica 
			# de acceso al dato diferente, cada tracker poller debe de implementar un metodo para poder obtener
			# la informacion correspondiente. Por ejemplo una API puede retornar un objeto complejo
			# mientras que otra puede retornar simplemente un Hash.
			def api_extract_delivery_status(api_result)
				raise NotImplementedError, "#{self.class} must implement the method '#{__method__}'"
			end

			# Retorna una instancia de una clase al que se llamara el metodo especificado por api_tracking_method_name
			# con los argumentos especificados por api_tracking_method_params. Funciona como un factory method.
			def api_tracking_object
				raise NotImplementedError, "#{self.class} must implement the method '#{__method__}'"
			end

			def api_tracking_method_params(tracking_number) 
				raise NotImplementedError, "#{self.class} must implement the method '#{__method__}'"
			end

			# Retorna una instancia del TrackerStateMapper correspondiente al proveedor de logistica
			# especifica
			def delivery_status_mapper
				raise NotImplementedError, "#{self.class} must implement the method '#{__method__}'"
			end
		end
	end
end

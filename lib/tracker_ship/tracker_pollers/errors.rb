module TrackerShip
	module TrackerPoller
		module Error
			class AuthenticationFailed < StandardError; end
			class MissingRequiredParameter < StandardError; end
		end
	end
end

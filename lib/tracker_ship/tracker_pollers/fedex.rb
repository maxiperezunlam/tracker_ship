require_relative './base'
require 'fedex'

module TrackerShip
	module TrackerPoller
		class Fedex < TrackerShip::TrackerPoller::Base
			def api_tracking_object
				begin
					@api_tracking_object ||= 
						::Fedex::Shipment.new(key: @configuration_service[:key],
			       					    password: @configuration_service[:password],
						                    account_number: @configuration_service[:account_number],
						                    meter: @configuration_service[:meter],
						                    mode: @configuration_service[:mode])
				rescue ::Fedex::RateError => e
					case e.message
					when /\AAuthentication Failed\z/i
						raise TrackerShip::TrackerPoller::Error::AuthenticationFailed, "Invalid credentials"
					when /\AMissing Required Parameter .+\z/i
						raise TrackerShip::TrackerPoller::Error::MissingRequiredParameter,
						      "No se ha podido construir exitosamente #{__method__}"
					end
				end
			end

			def api_tracking_method_name
				:track
			end

			def api_extract_delivery_status(api_result)
				api_result.first.status	
			end

			def api_tracking_method_params(tracking_number)
				{ tracking_number: tracking_number }
			end
			
			def delivery_status_mapper
				TrackerShip::TrackerStateMapper::Fedex.new			
			end
		end
	end
end

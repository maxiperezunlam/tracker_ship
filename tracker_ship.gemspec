# frozen_string_literal: true

require_relative "lib/tracker_ship/version"

Gem::Specification.new do |spec|
  spec.name          = "tracker_ship"
  spec.version       = TrackerShip::VERSION
  spec.authors       = ["Maximiliano Perez"]
  spec.email         = ["maxiperezunlam@gmail.com"]

  spec.summary       = "Biblioteca con clases comunes para consultar el estado de envío un paquete en diferentes proveedores de logistica"
  spec.description   = 
<<HEREDOC
	La gema cuenta con dos tipos entidades fundamentales:
		* TrackerStateMapper: El proposito es homogenizar los estados de los diferentes proveedores en un conjunto de estados 
                  comunes a ser utilizados por aplicaciones de terceros. Actualmente solo se implementará la clase base y el
		  TrackerStateMapper::Fedex.
		* TrackerPoller: El proposito es realizar las consultas a los diferentes proveedores de logistica dando al programador
		  una interfaz común. Actualmente simplemente se implementarán las clases base de la funcionalidad y el TrackerPoller::Fedex 

        DISCLAIMER

	El proposito de esta gema es centralizar la logica de consulta de los estados de paquetes de diferentes proveedores
	de logistica. La misma SOLAMENTE tiene el proposito de centralizar logica en pruebas de concepto de diferentes 
        enfoques de comunicacion asincronica utilizando Ruby. No debera de utilizar para proyectos productivos reales, ya 
        que no se le dará soporte alguno.
HEREDOC

  spec.homepage      = "https://bitbucket.org/maxiperezunlam/tracker_ship/"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")
  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://bitbucket.org/maxiperezunlam/tracker_ship/src/master/"
  spec.metadata["changelog_uri"] = "https://bitbucket.org/maxiperezunlam/tracker_ship/src/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features|\.circleci|\.git)/}) }
  end
  Dir.glob(File.join(File.expand_path(__dir__), "lib", "**", "*.rb")).each do |file|
  	spec.files << file
  end 

  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = "lib"  

  spec.add_dependency "fedex", "~> 3.10"
  spec.add_dependency "json", "~> 2.5"
  spec.add_dependency "dry-monads", "~> 1.3"
  spec.add_dependency "em-synchrony", "~> 1.0"
  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
